FROM alpine:edge
RUN apk add --no-cache python3
RUN apk add --no-cache ansible
RUN apk add --no-cache ansible-lint
